<a class="far fa-trash-alt icon-possition" data-toggle="modal" data-target="#modal-number{{ $task->id }}-destroy"></a>
<!-- 1.モーダルの配置 -->
<div class="modal modal-2"" id="modal-number{{ $task->id }}-destroy" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- 2.モーダルのコンテンツ -->
        <div class="modal-content">
            <!-- 3.モーダルのヘッダ -->
            <div class="modal-header">
                <h4 class="modal-title text-center" id="modal-label">確認</h4>
            </div>
            <!-- 4.モーダルのボディ -->
            <div class="modal-body">
                <p>
                    本当に削除しますか？
                </p>
            </div>
            <!-- 5.モーダルのフッタ -->
            <div class="modal-footer center-block destroy-modal-button-possition ">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">閉じる</button>
                <form action="{{ route('task.destroy', ['id' => $task->folder_id, 'task_id' => $task->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger pull-right"  value="削除" class="btn btn-danger btn-sm btn-dell">
                </form>
            </div>
        </div>
    </div>
</div>