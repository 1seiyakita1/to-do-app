

$(function() {
    $('#sortable').sortable({
        update: function() {
        var ids = $(this).sortable('toArray');
        console.log( ids );
        // console.log( JSON.stringify(ids) );
        // var json = {
        //     "sort_ids" : ids
        // }
        var postUrl= location.href + "/sort";
        console.log( postUrl );

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: postUrl,
            type:'POST',
            dataType: 'json',
            data: {
                'sort_ids': ids
            },
            timeout:3000,
        }).done(function(data) {

        }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
            alert("並び変えできませんでした");
        })
        }
    });

});