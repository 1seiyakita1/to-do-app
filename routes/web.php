<?php

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/folders/create', 'FolderController@showCreateForm')->name('folder.create');
    Route::post('/folders/create', 'FolderController@create');

    Route::group(['middleware' => 'can:view,folder'], function() {
        Route::get('/folders/{folder}/tasks', 'TaskController@index')->name('tasks.index');

        Route::get('/folders/{folder}/tasks/create', 'TaskController@showCreateForm')->name('task.create');
        Route::post('/folders/{folder}/tasks/create', 'TaskController@create');

        Route::get('/folders/{folder}/tasks/{task}/edit', 'TaskController@showEditForm')->name('task.edit');
        Route::post('/folders/{folder}/tasks/{task}/edit', 'TaskController@edit');

        Route::post('/folders/{folder}/tasks/{task}/destroy', 'TaskController@destroy')->name('task.destroy');

        Route::post('/folders/{folder}/tasks/sort', 'TaskController@sort')->name('tasks.sort');
    });
});
Auth::routes();